Dockerizando uma aplicação ASP NET.


- Passo 1: Criação de Máquina "Windows Datacenter 2019" no google cloud. Selecionem a opção Desktop
- Passo 2: Instalação Docker: https://computingforgeeks.com/how-to-run-docker-containers-on-windows-server-2019/ 
- Passo3: Dockerização. Material completo aqui: https://www.wintellect.com/migrating-legacy-asp-net-apps-to-docker/

Em  resumo:

1 - Download da aplicação de exemplo ASP Net (Vocês podem desenvolver uma ou usar uma existente):
http://www.wintellect.com/devcenter/wp-content/uploads/2017/05/docker-app.zip
Essa aplicação já é o pacote gerado.

Download da imagem base: docker pull microsoft/aspnet:3.5

Entrar na pasta baixada no passo 1 e executar o Dockerfile:
docker build --no-cache -t myoldschoolapp .

Rodando a aplicação:

docker run -dit --name myoldschoolapp1 -p 80:80 myoldschoolapp

Depois é só pegar o ip público da máquina. Por exemplo:

http://34.67.232.206/WebForm1.aspx